package se_col;

import javax.swing.*;


class Colocviu extends JFrame {


    JLabel first, second;
    JTextField text1, text2;
    JButton button;


    Colocviu() {

        setTitle("ADD");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 250);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        first = new JLabel("First field");
        first.setBounds(15,50 , width, height);

        second = new JLabel("Second field");
        second.setBounds(15, 100, width, height);

        text1 = new JTextField();
        text1.setBounds(70, 50, width, height);

        text2 = new JTextField();
        text2.setEditable(false);
        text2.setBounds(100, 100, width, height);

        button = new JButton("ADD");
        button.setBounds(10, 150, width, height);
        button.addActionListener(e -> {
            String s3=text1.getText();
            text2.setText(s3);
        });

        add(first);
        add(second);
        add(text1);
        add(text2);
        add(button);

    }

    public static void main(String[] args) {
        new Colocviu();
    }
}